﻿using UnityEngine;
using System.Collections.Generic;

public enum Suit
{
    hearts = 1,
    spades = 2,
    diamonds = 3,
    clubs = 4
}



public struct card
{
    public Suit suit;
    public int number;
}

public struct cardCount
{
    public Suit suit;
    public int number;
}



public class CardHandler {

     static List<int> suitCount = new List<int>() { 1, 2, 3, 4 };

     static List<int> heartsCount = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
     static List<int> spadesCount = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
     static List<int> diamondsCount = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
     static List<int> clubsCount = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };


     static Dictionary<Suit, List<int>> cardsCount = new Dictionary<Suit, List<int>>()
                                                                                {
                                                                                    { Suit.hearts, heartsCount },
                                                                                    { Suit.spades, spadesCount },
                                                                                    { Suit.diamonds, diamondsCount },
                                                                                    { Suit.clubs, clubsCount }
                                                                               };
    public static List<card> playerCard = new List<card>();
    public static List<card> oppositePlayerCard = new List<card>();
    public static List<card> leftPlayerCard = new List<card>();
    public static List<card> rightPlayerCard = new List<card>();



    public static card GenerateRandomCard()
    {
        card generatedCard;

        int suit;

        again:

        //Get the random suit
        suit = Random.Range(1, suitCount.Count);
        generatedCard.suit = (Suit)suit;


        List<int> randomedSuit = cardsCount[generatedCard.suit];

        if (randomedSuit.Count > 0)
        {
            //Get the random card from random suit
            generatedCard.number = randomedSuit[ Random.Range(1, randomedSuit.Count)];
            cardsCount[generatedCard.suit].Remove(generatedCard.number);
        }
        else
        {
            //If random suit have no card left, randomize suit from remaining suits
            suitCount.Remove(suit);
            goto again;
        }

        return generatedCard;
    }
}
