﻿using UnityEngine;
using DG.Tweening;

public class GameManager : MonoBehaviour {

    public GameObject panelNewCard;
    public GameObject playerHand;

    card card;
    Sequence cardSequence;

    void Start ()
    {
        DistributeCards();
    }

    public void GetCardFromDeck()
    {
        card = CardHandler.GenerateRandomCard();


        GameObject loadCardFromDisk = Resources.Load(card.suit.ToString() + " " + card.number.ToString()) as GameObject;
        //GameObject loadCardFromDisk = Resources.Load("PlaceHolder") as GameObject;
        GameObject loadedCard = Instantiate(loadCardFromDisk, panelNewCard.transform.position, Quaternion.identity) as GameObject;
        loadedCard.transform.SetParent(panelNewCard.transform);
        loadedCard.transform.localScale = Vector3.one;
        loadedCard.GetComponent<UIWidget>().depth = 3;
        loadedCard.name = card.suit.ToString() + " " + card.number.ToString();
    }


    public void DistributeCards()
    {
       cardSequence = DOTween.Sequence();

        for (int j = 0; j < 4; j++)
        {
            for (int i = 0; i < 8; i++)
            {
                if(j == 0)
                    CardToPlayerHand();
                else
                {
                    CardToOpponents();
                }
            }
            if(j == 0)
                Invoke("Rearrange", 0.3f);

        }
    }

    void CardToOpponents()
    {
        card = CardHandler.GenerateRandomCard();

        if (CardHandler.leftPlayerCard.Count <= 8)
            CardHandler.leftPlayerCard.Add(card);
        else if (CardHandler.oppositePlayerCard.Count <= 8)
            CardHandler.oppositePlayerCard.Add(card);
        else if (CardHandler.rightPlayerCard.Count <= 8)
            CardHandler.rightPlayerCard.Add(card);
    }

    void CardToPlayerHand()
    {
        card = CardHandler.GenerateRandomCard();

        CardHandler.playerCard.Add(card);

        GameObject loadCardFromDisk = Resources.Load("FaceDownCard") as GameObject;
        GameObject loadedCard = Instantiate(loadCardFromDisk, panelNewCard.transform.position, Quaternion.identity) as GameObject;
        loadedCard.transform.SetParent(panelNewCard.transform);
        loadedCard.transform.localScale = Vector3.one;
        loadedCard.GetComponent<UIWidget>().depth = 3;
        loadedCard.name = card.suit.ToString() + " " + card.number.ToString();
        //   loadedCard.GetComponent<TweenPosition>().value = playerHand.transform.position;
        playerHand.GetComponent<UIDragScrollView>().scrollView.ResetPosition();
//        cardSequence.Append(loadedCard.transform.DOMove(playerHand.transform.position, 0.2f, false));
        loadedCard.transform.SetParent(playerHand.GetComponent<UIDragDropContainer>().reparentTarget.transform);
  //      playerHand.GetComponent<UIDragDropContainer>().reparentTarget.GetComponent<UIGrid>().Reposition();
    }

    void Rearrange()
    {
        playerHand.GetComponent<UIDragDropContainer>().reparentTarget.GetComponent<UIGrid>().Reposition();
        playerHand.GetComponent<UIDragScrollView>().scrollView.ResetPosition();
    }

}
